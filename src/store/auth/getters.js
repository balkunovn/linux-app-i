export const isLoggedIn = ({ state }) => !!state.token

export function status ({ state }) {
  return state.status
}

export function token ({ state }) {
  return state.token
}
