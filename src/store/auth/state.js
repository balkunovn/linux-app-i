import { machineIdSync } from 'node-machine-id';

export default function () {
  return {
    state: {
      status: '',
      token: '',
      refreshToken: localStorage.getItem('refreshToken') || '',
      fingerprint: 'L' + machineIdSync(true).match(/.{1,8}/g).join('-')
    }
  }
}
