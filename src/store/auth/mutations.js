export function authRequest ({ state }) {
  state.status = 'loading'
  state.token = ''
}

export function authSuccess ({ state }, { token, refreshToken }) {
  state.status = 'success'
  state.token = token
  state.refreshToken = refreshToken
}

export function authError ({ state }) {
  state.status = 'error'
  state.token = ''
}

export function authLogout ({ state }) {
  state.status = 'logout'
  state.token = ''
  state.refreshToken = ''
}
