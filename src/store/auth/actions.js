import http from "../../services/httpBrowser"
import Auth from '../../services/api/auth'
import router from "src/router";


export async function login ({ commit, state, dispatch }, { login, passwd }) {
  commit('authRequest')

  const authApi = new Auth()

  const fingerprint = state.state.fingerprint
  const { token, refreshToken } = await authApi.login({ login, passwd, fingerprint })
    .catch(err => Promise.reject(err))

  localStorage.setItem('refreshToken', refreshToken)
  http.setAuthHeader(token)

  commit('authSuccess', { token, refreshToken })
}

export async function refresh ({ commit, state, dispatch }) {
  commit('authRequest')

  const authApi = new Auth()

  const fingerprint = state.state.fingerprint
  const refreshTokenOld = state.state.refreshToken

  const { token, refreshToken } = await authApi.refresh({ refreshToken: refreshTokenOld, fingerprint })
    .catch(err => Promise.reject(err))

  localStorage.setItem('refreshToken', refreshToken)
  http.setAuthHeader(token)

  commit('authSuccess', { token, refreshToken })
}

export async function logout ({ commit, state }) {
  const authApi = new Auth()

  const fingerprint = state.state.fingerprint
  const refreshToken = state.state.refreshToken

  http.setAuthHeader('')
  localStorage.clear()

  router.push('/preview').catch((err) => {})

  if (state.state.status !== 'logout') {
    authApi.logout({ refreshToken, fingerprint })
      .catch((err) => Promise.reject(err))
  }

  commit('authLogout')
}
