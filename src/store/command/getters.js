export function serial ({ state }) {
  return state.serial
}

export function hostname ({ state }) {
  return state.hostname
}

export function refresh ({ state }) {
  return state.refresh
}

export function timezone ({ state }) {
  return state.timezone
}

export function weather ({ state }) {
  return state.weather
}

export function logo ({ state }) {
  return state.logo
}

export function phone ({ state }) {
  return state.phone
}

export function email ({ state }) {
  return state.email
}

export function subtextright ({ state }) {
  return state.subtextright
}

export function qrcodetext ({ state }) {
  return state.qrcodetext
}

export function advertising ({ state }) {
  return state.advertising
}
