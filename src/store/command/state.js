import { machineIdSync } from 'node-machine-id';
import fs from 'fs'

export default function () {
  let hostname = null

  if (fs.existsSync('hostname')) {
    hostname = fs.readFileSync('hostname', 'utf8')
  }

  return {
    state: {
      hostname: hostname || process.env.hostname,
      refresh: 10,
      // serial: "L10fe259a-2ad54dae-9cb7d54c-55f851b8" || 'L' + machineIdSync(true).match(/.{1,8}/g).join('-'),
      serial: 'L' + machineIdSync(true).match(/.{1,8}/g).join('-'),
      weather: { temperature : null, iconCode: null, icon: null },
      timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      logo: '',
      phone: '',
      email: '',
      subtextright: '',
      qrcodetext: '',
      advertising: []
    }
  }
}
