import fs from 'fs'

export function hostname ({ state }, { hostname }) {
  try {
    const url = new URL(hostname)
    hostname = `http://${url.hostname + url.pathname}`
  } catch (e) { hostname = null }

  if (!hostname) hostname = process.env.hostname

  fs.writeFileSync('hostname', hostname)
  state.hostname = hostname
}

export function refresh ({ state }, { refresh }) {
  if (refresh < 1) refresh = 1

  state.refresh = refresh
}

export function timezone({ state }, { timezone }) {
  try {
    new Date().toLocaleString('ru', { timeZone: timezone })
    state.timezone = timezone
  } catch(e) { }
}

export function weather ({ state }, { temperature, iconCode }) {
  const time = new Date().toLocaleTimeString("ru", { timeZone: state.timezone })
  const hours = parseInt(time.split(':')[0])

  const iconSuffix = (hours < 6 || hours > 18) ? 'n.png' : 'd.png'
  switch (+iconCode) {
    case 200: case 201: case 202: case 210: case 211: case 212: case 221: case 230: case 231: case 232:
      state.weather.icon = `11${iconSuffix}`; break;
    case 300: case 301: case 302: case 310: case 311: case 312: case 313: case 314: case 321: case 520: case 521: case 522: case 531:
      state.weather.icon = `09${iconSuffix}`; break;
    case 500: case 501: case 502: case 503: case 504:
      state.weather.icon = `10${iconSuffix}`; break;
    case 511: case 600: case 601: case 602: case 611: case 612: case 613: case 615: case 616: case 620: case 621: case 622:
      state.weather.icon = `13${iconSuffix}`; break;
    case 701: case 711: case 721: case 731: case 741: case 751: case 761: case 762: case 771: case 781:
      state.weather.icon = `50${iconSuffix}`; break;
    case 800:
      state.weather.icon = `01${iconSuffix}`; break;
    case 801:
      state.weather.icon = `02${iconSuffix}`; break;
    case 802:
      state.weather.icon = `03${iconSuffix}`; break;
    case 803: case 804:
      state.weather.icon = `04${iconSuffix}`; break;
    default: state.weather.icon = iconCode + '.png'
  }

  state.weather.temperature = temperature
  state.weather.iconCode = iconCode
}

export function logo({ state }, { path }) {
  if (state.logo !== path) state.logo = path
}

export function phone({ state }, { phone }) {
  if (state.phone !== phone) state.phone = phone
}

export function email({ state }, { email }) {
  if (state.email !== email) state.email = email
}

export function subtextright({ state }, { subtextright }) {
  if (state.subtextright !== subtextright) state.subtextright = subtextright
}

export function qrcodetext({ state }, { qrcodetext }) {
  if (state.qrcodetext !== qrcodetext) {
    state.qrcodetext = qrcodetext
  }
}

export function advertising({ state }, { advertising }) {
  const shallowEqual = (object1, object2) => {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
      return false;
    }

    for (let key of keys1) {
      if (Object.entries(object1[key]).toString() !== Object.entries(object2[key]).toString()) {
        return false;
      }
    }

    return true;
  }

  if (!shallowEqual(state.advertising, advertising)) {
    state.advertising = advertising
  }
}
