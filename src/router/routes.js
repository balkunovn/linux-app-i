const routes = [
  {
    path: '/',
    component: () => import('components/theme/Kosmopolit'),
  },
]

export default routes
