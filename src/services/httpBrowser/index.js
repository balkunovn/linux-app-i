import Http from './../http'
import Queue from './../helpers/queue'

import httpInterceptorsRequest from './httpInterceptorsRequest'
import httpInterceptorsResponse from './httpInterceptorsResponse'


const http = new Http()
const queue = new Queue()

httpInterceptorsRequest({ http })
httpInterceptorsResponse({ http, queue })

export default http
