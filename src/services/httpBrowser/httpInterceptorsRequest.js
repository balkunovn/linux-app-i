import pkg from "../../../package.json";


export default ({ http }) => {
  http.setInterceptorRequest(config => {
    config.baseURL = process.env.host
    config.headers['X-Requested-With'] = pkg.productName

    if (typeof config.headers.Authorization === "undefined") config.headers.Authorization = ''

    return config
  }, err => Promise.reject(err))
}
