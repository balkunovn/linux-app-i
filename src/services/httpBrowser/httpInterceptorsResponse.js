import Store from "../../store";


export default ({ http, queue }) => {

  const retryCurrentRequest = (originalRequest) => {
    originalRequest.headers.Authorization = Store.getters['auth/token']
    originalRequest._retry = true

    return http.client(originalRequest)
  }

  http.setInterceptorResponse(response => response, error => {
    const originalRequest = error.config

    if (error.response && error.response.status === 401) {
      if (originalRequest._refresh) Store.commit('auth/authError')

      if (['logout', 'error'].includes(Store.getters['auth/status'])) {
        Store.dispatch('auth/logout')
        return Promise.reject(error)
      }

      if (Store.getters['auth/token'] && originalRequest.headers.Authorization !== Store.getters['auth/token']) {
        return retryCurrentRequest(originalRequest)
      }

      if (!originalRequest._retry) {
        const promise = new Promise(queue.executor())
          .then(() => retryCurrentRequest(originalRequest))
          .catch(() => Promise.reject(error))

        if (!['logout', 'error', 'loading'].includes(Store.getters['auth/status'])) {
          Store.dispatch('auth/refresh')
            .then(() => queue.processSuccess())
            .catch(() => queue.processFailed())
        }

        return promise
      }
    }

    return Promise.reject(error)
  })
}
