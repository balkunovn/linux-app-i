import axios from 'axios'


export default class Http {
  constructor(options = {}) {
    this.client = options.client || axios.create()

    this.token = options.token || ''
  }

  async get(url, config = {}) {
    return await this.client.get(url, config)
      .catch(err => Promise.reject(err))
  }

  async post(url, body, config = {}) {
    const res = await this.client.post(url, body, config)
      .catch(err => Promise.reject(err))

    if (typeof res === 'object') return res

    return Promise.reject()
  }

  setAuthHeader(token) {
    this.client.defaults.headers.Authorization = token
  }

  setInterceptorRequest(interceptorSuccess, interceptorError) {
    this.client.interceptors.request.use(interceptorSuccess, interceptorError)
  }

  setInterceptorResponse(interceptorSuccess, interceptorError) {
    this.client.interceptors.response.use(interceptorSuccess, interceptorError)
  }
}
