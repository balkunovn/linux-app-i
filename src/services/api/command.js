import httpElectron from './../httpElectron'

export default class Command {
  constructor({ serial }) {
    this.serial = serial
  }

  async get() {
    const { data } = await httpElectron.post('', `msn=${this.serial}&monitor=1`)
      .catch((e) => Promise.reject(e))

    return data
  }
}
