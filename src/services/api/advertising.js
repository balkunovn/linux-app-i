import httpElectron from './../httpElectron'


const API_URL = process.env.host + '/api/advertising.php'

import Store from 'src/store'
import {CommandHandler} from "src/services/command";

export default class AdvertisingApi {
  static STATUS_NOTSET = 0
  static STATUS_RUN = 1
  static STATUS_STOP = 2

  constructor(serial) {
    this.serial = serial
    this.status = this.constructor.STATUS_NOTSET
  }

  init () {
    this.status = this.constructor.STATUS_RUN

    return this.handCommands().catch((e) => {})
  }

  async handCommands() {
    const response = await httpElectron.post(API_URL, `msn=${this.serial}&monitor=1`)
      .catch((e) => { console.log(e) })

    if (response && response.data) {
      this.commandsRun(response.data)
    }

    return this.handCommandsTimeout()
  }

  handCommandsTimeout() {
    clearTimeout(this.tId)

    if (this.status === this.constructor.STATUS_RUN) {
      return this.tId = setTimeout(this.handCommands.bind(this), Store.getters['command/refresh'] * 1000)
    }
  }

  commandsRun(data) {
    try {
      const responseData = JSON.parse(data)
      const responseDataEntries = Object.entries(responseData)

      responseDataEntries.map(([command, params]) => {
        if (command === "__setadvertising") {
          new CommandHandler(command, ...Object.values(params))
        }
      })

    } catch (e) { }
  }

  stop() {
    this.status = this.constructor.STATUS_STOP
  }
}
