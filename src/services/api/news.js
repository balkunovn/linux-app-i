import httpElectron from './../httpElectron'


export default class News {
  static STATUS_NOTSET = 0
  static STATUS_RUN = 1
  static STATUS_STOP = 2

  constructor(onUpdate) {
    this.onUpdate = onUpdate
    this.status = this.constructor.STATUS_NOTSET
    this.ttl = 1
  }

  init () {
    this.status = this.constructor.STATUS_RUN

    return this.handCommands().catch((e) => {})
  }

  async handCommands() {

    const response = await httpElectron.get("https://ru.krymr.com/api/zgtopme_iip_")
      .catch((e) => { console.log(e) })

    if (response && response.data) {
      this.commandsRun(response.data)
    }

    return this.handCommandsTimeout()
  }

  handCommandsTimeout() {
    clearTimeout(this.tId)
    if (this.status === this.constructor.STATUS_RUN) {
      return this.tId = setTimeout(this.handCommands.bind(this), (this.ttl || 1) * 1000 * 60)
    }
  }

  commandsRun(data) {
    const res = []
    data = new window.DOMParser().parseFromString(data, "text/xml")
    this.ttl = +data.querySelector("ttl").innerHTML
    const items = data.querySelectorAll("item")
    items.forEach(item => {
      const title = item.querySelector("title").innerHTML
      res.push(title)
    })

    this.onUpdate(res)
  }

  stop() {
    this.status = this.constructor.STATUS_STOP
  }
}
