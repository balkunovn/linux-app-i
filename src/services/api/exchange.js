import httpElectron from './../httpElectron'


export default class Exchange {
  static STATUS_NOTSET = 0
  static STATUS_RUN = 1
  static STATUS_STOP = 2

  constructor(onUpdate) {
    this.onUpdate = onUpdate
    this.status = this.constructor.STATUS_NOTSET
    this.ttl = 60 * 60 * 1000
  }

  init () {
    this.status = this.constructor.STATUS_RUN

    return this.handCommands().catch((e) => {})
  }

  async handCommands() {

    const response = await httpElectron.get("https://www.cbr-xml-daily.ru/daily_json.js")
      .catch((e) => { console.log(e) })

    if (response && response.data) {
      this.commandsRun(response.data)
    }

    return this.handCommandsTimeout()
  }

  handCommandsTimeout() {
    clearTimeout(this.tId)
    if (this.status === this.constructor.STATUS_RUN) {
      return this.tId = setTimeout(this.handCommands.bind(this), this.ttl)
    }
  }

  commandsRun(data) {
    data = JSON.parse(data)
    const valutes = data.Valute

    const USD = valutes.USD.Value
    const EUR = valutes.EUR.Value

    this.onUpdate({USD, EUR})
  }

  stop() {
    this.status = this.constructor.STATUS_STOP
  }
}
