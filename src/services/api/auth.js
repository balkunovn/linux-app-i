import http from '../httpBrowser'


export default class Auth {
  async login({ login, passwd, fingerprint }) {
    const { data } = await http.post('api/auth/login/index.php', { login, passwd, fingerprint })
      .catch(err => Promise.reject(err))

    const { jwt, refresh_token } = data
    return {
      token: jwt,
      refreshToken: refresh_token
    }
  }

  async refresh({ refreshToken, fingerprint }) {
    const { data } = await http.post(
      '/api/auth/refresh/index.php',
      { refreshToken, fingerprint },
      { _refresh: 1 }
      )
      .catch(err => Promise.reject(err))

    const { jwt, refresh_token } = data

    return {
      token: jwt,
      refreshToken: refresh_token
    }
  }

  async logout({ refreshToken, fingerprint }) {
    await http.post('api/auth/logout/index.php', { refreshToken, fingerprint })
  }
}
