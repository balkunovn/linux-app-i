import Store from "src/store"

export default class DateTime {
  constructor(options = {}) {
    this.timezone = options.timezone || null
    this.date = options.date || new Date()
  }

  getFullDate() {
    return this.date.toLocaleString("ru", {timeZone: 'Europe/Kiev'})
  }

  getDay() {

  }

}
