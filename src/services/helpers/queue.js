export default class Queue {
  constructor() {
    this.queue = []
  }

  add(resolve, reject) {
    this.queue.push({ resolve, reject })
  }

  executor() {
    return (resolve, reject) => {
      this.queue.push({ resolve, reject })
    }
  }

  processSuccess(...args) {
    this.queue.forEach((q) => q.resolve(...args))
    this.queue = []
  }

  processFailed(...args) {
    this.queue.forEach((q) => q.reject(...args))
    this.queue = []
  }
}
