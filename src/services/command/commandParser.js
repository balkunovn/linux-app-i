export default class CommandParser {
  parseToStringCommand(data) {
    return data.split("\n")
  }

  parseStringCommandToArray(cmd) {
    return cmd.replace(/(\t|\s\s+)/g, ' ').split(' ')
  }
}
