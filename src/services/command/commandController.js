import Store from 'src/store'
import CommandApi from 'src/services/api/command'
import { CommandParser, CommandHandler } from 'src/services/command'

export default class CommandController {
  static STATUS_NOTSET = 0
  static STATUS_RUN = 1
  static STATUS_STOP = 2

  constructor() {
    this.status = this.constructor.STATUS_NOTSET

    this.parser = new CommandParser()

    this.commandApi = new CommandApi({
      serial: Store.getters['command/serial']
    })
  }

  init () {
    this.status = this.constructor.STATUS_RUN
    console.log('init')

    return this.handCommands().catch((e) => {})
  }

  async handCommands() {
    const data = await this.commandApi.get().catch(e => {})

    if (data) {
      this.commandsRun(data)
    }

    return this.handCommandsTimeout()
  }

  handCommandsTimeout() {
    clearTimeout(this.tId)
    if (this.status === this.constructor.STATUS_RUN) {
      return this.tId = setTimeout(this.handCommands.bind(this), Store.getters['command/refresh'] * 1000)
    }
  }

  commandsRun(data) {
    this.parser.parseToStringCommand(data).forEach((cmd) => {
      const command = this.parser.parseStringCommandToArray(cmd)
        new CommandHandler(command.shift(), ...command)
    })
  }

  stop() {
    console.log('stop')

    this.status = this.constructor.STATUS_STOP
  }
}
