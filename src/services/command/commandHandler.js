import { exec } from 'child_process'
import Store from 'src/store'
import { base64Decode } from 'src/services/helpers/base64'

export default class CommandHandler {
  constructor(command, ...params) {
    if (typeof this[command] === "function") {
      this[command](...params)
    } else if (command) {
      console.log(`"${command}"`, 'command not exist')
    }
  }

  __demo(login, passwd) {
    console.log(login, passwd)
  }

  __seturl(hostname = "") {
    Store.commit('command/hostname', { hostname })
  }

  __settmzone(timezone) {
    Store.commit('command/timezone', { timezone })
  }

  __refresh(refresh = 10) {
    Store.commit('command/refresh', { refresh })
  }

  __reboot_now() {
    exec('reboot')
  }

  weather(temperature, iconCode) {
    Store.commit('command/weather', { temperature, iconCode })
  }

  __announcement(data) {
    try {
      const announcements = JSON.parse(base64Decode(data))
      Store.commit('announcement/set', { announcements })
    } catch (e) { }
  }

  __set_logo(logopath_base64) {
    const path = base64Decode(logopath_base64)
    Store.commit('command/logo', { path })
  }

  __set_phone(phone_base64) {
    const phone = base64Decode(phone_base64)
    Store.commit('command/phone', { phone })
  }

  __set_email(email_base64) {
    const email = base64Decode(email_base64)
    Store.commit('command/email', { email })
  }

  __set_subtextright(subtextright_base64) {
    const subtextright = base64Decode(subtextright_base64)
    Store.commit('command/subtextright', { subtextright })
  }

  __set_qrcodetext(qrcodetext_base64) {
    const qrcodetext = base64Decode(qrcodetext_base64)
    Store.commit('command/qrcodetext', { qrcodetext })
  }

  __qrcodetext(qrcodetext) {
    Store.commit('command/qrcodetext', { qrcodetext })
  }

  __setadvertising(...advertising) {
    Store.commit('command/advertising', { advertising })
  }
}
