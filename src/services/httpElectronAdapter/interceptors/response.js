export default class Response {
  use(onSuccess, onError) {
    this.onSuccess = onSuccess
    this.onError = onError
  }

  onSuccess(success) {
    return success
  }

  onError(err) {
    return err
  }
}

