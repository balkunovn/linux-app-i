import Request from './request'
import Response from './response'


export default { request: new Request(), response: new Response() }
