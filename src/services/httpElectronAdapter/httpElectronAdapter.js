const { net }  = require('@electron/remote')

import interceptors from './interceptors'

export default class HttpElectronAdapter {
  constructor() {
    this.interceptors = interceptors
    this.defaults = {
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      timeout: 1000
    }
  }

  async request(options, body = "") {
    try {
      options = Object.assign(this.defaults, options)


      // if (options.hostname && options.url) {
      //   options.path = options.url
      //   delete options.url
      // }

      const request = net.request(this.interceptors.request.onSuccess(options))

      const promise = new Promise((pResponse, pReject) => {
        request.end(body);

        request.on('response', (response) => {
          if (response.statusCode >= 200 && response.statusCode < 300) {
            response.on('data', (chunk) => {
              return pResponse(this.interceptors.response.onSuccess({
                data: chunk.toString(),
                headers: response.headers,
                status: response.statusCode,
                config: options
              }))
            })
          } else {
            request.emit('error', {
              message: 'net::ERR_STATUS_NOT_OK'
            })
          }
        })

        request.on('error', error => {
          error.config = options
          return pReject(this.interceptors.response.onError(error))
        })
      })

      return await promise.then(response => response).catch(err => Promise.reject(err))
    } catch (error) {
      return this.interceptors.request.onError(error)
    }
  }

  async get(url = "", config = {}) {
    config.method = 'GET'
    config.url = url

    return await this.request(config, '')
  }

  async post(url = "", data = '', config = {}) {
    config.method = 'POST'
    config.url = url

    return await this.request(config, data)
  }
}
