import Store from 'src/store'

export default ({ http }) => {
  http.setInterceptorRequest(config => {
    try {
      new URL(config.url)
    } catch (e) {
      config.url  = Store.getters['command/hostname'] + config.url
    }
    return config
  }, err => Promise.reject(err))
}
