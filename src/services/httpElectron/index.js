import httpElectronAdapter from "src/services/httpElectronAdapter";
import Http from "src/services/http/http";

import httpElectronInterceptorsRequest from "./httpElectronInterceptorsRequest";
import httpElectronInterceptorsResponse from "./httpElectronInterceptorsResponse";


const http = new Http({
  client: httpElectronAdapter
})

httpElectronInterceptorsRequest({ http })
httpElectronInterceptorsResponse({ http })

export default http
